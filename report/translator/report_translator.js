/**
 * Created by p24 on 25.05.2017.
 */
'use strict'

var replace = require('replace-in-file');

const options = {

    //Single file
    files: '',

    from: [],
    to: [],

    //Specify if empty/invalid file paths are allowed (defaults to false)
    //If set to true these paths will fail silently and no error will be thrown.
    allowEmptyPaths: false,

    //Character encoding for reading/writing files (defaults to utf-8)
    encoding: 'utf8',
};

module.exports = {
    translate(language, files) {

        options.files = files;
        var languageJSON = require('./languages/' + language + '.json');

        for (var key in languageJSON) {
            options.from.push(new RegExp(key, ['g']));
            options.to.push(languageJSON[key]);
        }

        try {
            let changedFiles = replace.sync(options);
            console.log('Modified files:', changedFiles.join(', '));
        }
        catch (error) {
            console.error('Error occurred:', error);
        }
    }
}