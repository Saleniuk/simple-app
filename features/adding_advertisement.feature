# language: en
Feature: : adding an advertisement

  @web-test
  Scenario: : adding new advertisement
    Given I am on the main page
    When I click the add advertisement button
    Then I should see the form to add an advertisement
