import { Component } from '@angular/core';
import './app.component.html';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {}
