/**
 * Created by p24 on 17.04.2017.
 */
import { NgModule } from '@angular/core';
import { TopMenuComponent } from './components/top-menu';

@NgModule({
    imports: [],
    declarations: [
        TopMenuComponent
    ],
    exports: [
        TopMenuComponent
    ]
})
export class SharedModule {}
