/**
 * Created by p24 on 23.04.2017.
 */
export const NumberConverter = (value: any) => {
    if (value === null || value === undefined || typeof value === 'number') {
        return value;
    }

    return parseFloat(value.toString());
};