import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HomeModule } from './home';
// import { BxSliderDirective } from './shared';

const rootRouting: ModuleWithProviders = RouterModule.forRoot([], { useHash: true });

@NgModule({
  declarations: [
      AppComponent,
      // BxSliderDirective
  ],
  imports: [
      HomeModule,
      BrowserModule,
      FormsModule,
      HttpModule,
      rootRouting
  ],
  providers: [],
  bootstrap: [
      AppComponent
  ]
})
export class AppModule { }
